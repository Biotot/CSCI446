$(document).ready(function(){

	var trueSelected = $('<input type="radio" name="option">');
	var falseSelected = $('<input type="radio" name="option">');
	var checkButton = $('<button>Check Answer</button>');
	$('.answer').append(trueSelected);
	$('.answer').append('True<br>');
	$('.answer').append(falseSelected);
	$('.answer').append('False<br>');
	$('.answer').append(checkButton);
	

	var Q1 = {lang: "C++", question: "hot pockets?", answer: false};
	var Q2 = {lang: "PHP", question: "Earth", answer: true};
	var Q3 = {lang: "Java", question: "Wind N Fire", answer: false};
	var Q4 = {lang: "BrainF**k", question: "Brain F**k was primarily designed as a joke", answer: true};
	var Qcurrent;
	var countCorrect = 0;
	
	$('.topicList').append('<div class = "topic" id = 1>' + Q1.lang + '</div>' );
	$('.topicList').append('<div class = "topic" id = 2>' + Q2.lang + '</div>' );
	$('.topicList').append('<div class = "topic" id = 3>' + Q3.lang + '</div>' );
	$('.topicList').append('<div class = "topic" id = 4>' + Q4.lang + '</div>' );
	
	$('.topic').hover(
	
		function(){
			$(this).css("background-color", "#0099FF");
		}
		,function(){
			$(this).css("background-color", "#00FFFF");
		}
	);
	
	$('.topic').click( 
		function(){
			$('.question').empty();
			
			if (this.id == 1){
				$('.question').append('<p> ' + Q1.question +  '</p>');
				Qcurrent = Q1;
			}
			if (this.id == 2){
				$('.question').append('<p> ' + Q2.question +  '</p>');
				Qcurrent = Q2;
			}
			if (this.id == 3){
				$('.question').append('<p> ' + Q3.question +  '</p>');
				Qcurrent = Q3;
			}
			if (this.id == 4){
				$('.question').append('<p> ' + Q4.question +  '</p>');
				Qcurrent = Q4;
			}
	});

	checkButton.click(function(){
		
		$(Qcurrent).hide();
		$('.sideBar').append('<p>' + Qcurrent.lang + '</p>');
		
		
		if (Qcurrent.answer == true){
			if (trueSelected.checked == true){
				countCorrect += 1;
			}
		}else{
			if (falseSelected.checked == true){
				countCorrect += 1;
			}
		}
	
	});
	
});