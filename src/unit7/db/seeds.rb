# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Pet.create(name: "shaggy dog", age: "twelve", description: "Found in the woods", pet_type: :unicorn);
Pet.create(name: "shaggy cat", age: "eighty", description: "really big angry cat", pet_type: :dog);
Pet.create(name: "Magic horse", age: "9", description: "big ugly horse", pet_type: :unicorn);
Pet.create(name: "applesauce", age: "14", description: "likes apples", pet_type: :cat);
Pet.create(name: "snowball", age: "31", description: "Found in the woods", pet_type: :cat);
Pet.create(name: "snowballII", age: "20", description: "Found in the woods", pet_type: :cat);
Pet.create(name: "snowballIII", age: "15", description: "Found in the woods", pet_type: :cat);
Pet.create(name: "snowballIX", age: "2", description: "Found in the woods", pet_type: :cat);