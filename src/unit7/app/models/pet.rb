class Pet < ActiveRecord::Base
	enum pet_type: [:dog, :cat, :unicorn]
	validates :name, presence: true
	validates :description, length: { in: 10..40}
end
