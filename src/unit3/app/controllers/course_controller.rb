class CourseController < ApplicationController
  def eval
  	@gradeTable = 
  		"<h1>Course Evaluation Criteria</h1>

  		<p> Participation - 5%<br>
  			Homework = 45%<br>
  			Exams - 50%<br>

  		</p>".html_safe
  end

  def announce
  	@date = Time.now();
  end

  def scores
  	redirect_to "/course/announce"
  end
end
