class AddBankIdColumnToDollars < ActiveRecord::Migration
  def change
    add_column :dollars, :bank_id, :integer
  end
end
