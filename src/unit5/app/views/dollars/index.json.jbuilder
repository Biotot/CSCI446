json.array!(@dollars) do |dollar|
  json.extract! dollar, :id, :value, :condition, :president
  json.url dollar_url(dollar, format: :json)
end
