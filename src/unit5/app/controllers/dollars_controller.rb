class DollarsController < ApplicationController
  before_action :set_dollar, only: [:show, :edit, :update, :destroy]

  # GET /dollars
  # GET /dollars.json
  def index
    @bank = Bank.find(params[:bank_id])
    @dollar = Dollar.all
  end

  # GET /dollars/1
  # GET /dollars/1.json
  def show
    @bank = Bank.find(params[:bank_id])
    @dollar = Dollar.find(params[:bank_id])
  end

  # GET /dollars/new
  def new
    @bank = Bank.find(params[:bank_id])
    @dollar = Dollar.new
  end

  # GET /dollars/1/edit
  def edit
    @bank = Bank.find(params[:bank_id])
  end

  # POST /dollars
  # POST /dollars.json
  def create
    @dollar = Dollar.new(dollar_params)

    respond_to do |format|
      if @dollar.save
        format.html { redirect_to [@dollar.bank, @dollar], notice: 'Dollar was successfully created.' }
        format.json { render :show, status: :created, location: @dollar }
      else
        format.html { render :new }
        format.json { render json: @dollar.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /dollars/1
  # PATCH/PUT /dollars/1.json
  def update
    respond_to do |format|
      if @dollar.update(dollar_params)
        @dollar.bank_id = 1;
        format.html { redirect_to [@dollar.bank, @dollar], notice: 'Dollar was successfully updated.' }
        format.json { render :show, status: :ok, location: @dollar }
      else
        format.html { render :edit }
        format.json { render json: @dollar.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dollars/1
  # DELETE /dollars/1.json
  def destroy
    @bank = Bank.find(params[:bank_id])
    @dollar = @bank.dollars.find(params[:id])
    @dollar.destroy
    respond_to do |format|
      format.html { redirect_to @bank, notice: 'Dollar was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_dollar
      @dollar = Dollar.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def dollar_params
      params.require(:dollar).permit(:value, :condition, :president, :bank_id)
    end
end
