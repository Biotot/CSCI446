# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Pet.create(name: "shaggy dog", age: "twelve", description: "Found in the woods", pet_type: :unicorn, pet_status: :available, picture: "catdog.jpg");
Pet.create(name: "shaggy cat", age: "eighty", description: "really big angry cat", pet_type: :dog, pet_status: :available, picture: "dog1.jpg");
Pet.create(name: "Magic horse", age: "9", description: "big ugly horse", pet_type: :unicorn, pet_status: :available, picture: "fox.jpg");
Pet.create(name: "applesauce", age: "14", description: "likes apples", pet_type: :cat, pet_status: :available, picture: "kitty.png");
Pet.create(name: "snowball", age: "31", description: "Found in the woods", pet_type: :cat, pet_status: :available, picture: "moocow.jpg");
Pet.create(name: "snowballII", age: "20", description: "Found in the woods", pet_type: :cat, pet_status: :available, picture: "pooped.jpg");
Pet.create(name: "snowballIII", age: "15", description: "Found in the woods", pet_type: :cat, pet_status: :available, picture: "sillydog.jpg");