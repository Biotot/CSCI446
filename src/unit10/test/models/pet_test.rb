require 'test_helper'

class PetTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
   test "pet attributes must not be empty" do
    pet = Pet.new
    assert pet.invalid?
  	assert pet.errors[:name].any?
  end
  test "blank name" do
    pet = Pet.new(name: pets(:noName).name, age: pets(:noName).age,
                  description: pets(:noName).description)
    assert pet.invalid?
    assert pet.errors[:name].any?
    assert_equal [I18n.translate('errors.messages.blank')],pet.errors[:name]
    puts pet.errors.messages
  end
  test "short description" do
    pet = Pet.new(name: pets(:length9).name, age: pets(:length9).age,
                  description: pets(:length9).description)
    assert pet.invalid?
    assert pet.errors[:description].any?
    puts pet.errors.messages
  end
  test "long description" do
    pet = Pet.new(name: pets(:length41).name, age: pets(:length41).age,
                  description: pets(:length41).description)
    assert pet.invalid?
    assert pet.errors[:description].any?
    puts pet.errors.messages
  end
  test "min description" do
    pet = Pet.new(name: pets(:length10).name, age: pets(:length10).age,
                  description: pets(:length10).description)
    assert !pet.invalid?
    assert !pet.errors[:description].any?
    puts pet.errors.messages
  end
  test "max description" do
    pet = Pet.new(name: pets(:length40).name, age: pets(:length40).age,
                  description: pets(:length40).description)
    assert !pet.invalid?
    assert !pet.errors[:description].any?
    puts pet.errors.messages
  end
end
