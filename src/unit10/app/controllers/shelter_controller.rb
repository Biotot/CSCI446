class ShelterController < ApplicationController
  include CurrentCart

  before_action :set_shelter, only: [:show, :edit, :update, :destroy]

  # GET /pets
  # GET /pets.json
  def index
  	#@shelter = Shelter.first
    @cart = set_cart
    #@pets = Pet.order(:pet_type)
    @pets = Pet.all.where(pet_status: 0).order(:pet_type)
  end
  #.filter(pet_status: :selected)
  # GET /pets/1
  # GET /pets/1.json
  def show
  end

  # GET /pets/new
  def new
  end

  # GET /pets/1/edit
  def edit
  end

  # POST /pets
  # POST /pets.json
  def create
  end

  # PATCH/PUT /pets/1
  # PATCH/PUT /pets/1.json
  def update
  end

  # DELETE /pets/1
  # DELETE /pets/1.json
  def destroy
  end

end
