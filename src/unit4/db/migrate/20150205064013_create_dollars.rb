class CreateDollars < ActiveRecord::Migration
  def change
    create_table :dollars do |t|
      t.integer :value
      t.string :condition
      t.string :president

      t.timestamps null: false
    end
  end
end
