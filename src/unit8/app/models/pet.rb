class Pet < ActiveRecord::Base
	enum pet_type: [:dog, :cat, :unicorn]
	enum pet_status: [:available, :selected, :adopted]
	validates :name, presence: true
	validates :description, length: { in: 10..40}
end
